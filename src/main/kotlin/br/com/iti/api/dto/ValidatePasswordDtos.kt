package br.com.iti.api.dto

data class ValidatePasswordInputDto(val password: String)
data class ValidatePasswordOutputDto(var valid: Boolean = false, var brokenRule: String? = null) {
    companion object {
        val OK = ValidatePasswordOutputDto(valid = true)
    }
}
