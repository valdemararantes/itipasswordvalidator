package br.com.iti.api.service

import br.com.iti.api.dto.ValidatePasswordInputDto
import br.com.iti.api.dto.ValidatePasswordOutputDto
import br.com.iti.api.rule.ValidationRule
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.ApplicationContext
import org.springframework.context.MessageSource
import org.springframework.stereotype.Service
import javax.annotation.PostConstruct

@Service
class PasswordValidatorService(
    private val messageSource: MessageSource,
    private val applicationContext: ApplicationContext
) {

    companion object {
        private val log = LoggerFactory.getLogger(PasswordValidatorService::class.java)
    }

    @Value("\${app.rules}")
    private lateinit var ruleBeanNames: Array<String>

    @PostConstruct
    fun showConfiguredRules() {
        log.info("Configured Rules: ${ruleBeanNames.asList()}")
    }

    fun validate(validatePasswordInputDto: ValidatePasswordInputDto): ValidatePasswordOutputDto {
        log.info("Executing...")

        val brokenRuleOutputDto = ruleBeanNames.asSequence()
            .map { ruleBeanName -> applicationContext.getBean(ruleBeanName) as ValidationRule }
            .map { ruleBean -> ruleBean.validate(validatePasswordInputDto) }
            .firstOrNull { outputDto -> !outputDto.valid }

        return brokenRuleOutputDto ?: ValidatePasswordOutputDto.OK
    }
}