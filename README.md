# ITIPasswordValidator

Projeto criado para o processo de seleção do ITI. É uma API REST que valida a senha segundo as seguintes regras:

- Nove ou mais caracteres
- Ao menos 1 dígito
- Ao menos 1 letra minúscula
- Ao menos 1 letra maiúscula
- Ao menos 1 caractere especial

O endpoint que serviço que segue a especificação do ITI estrita é:
 
`<domain:port>/rest/v1/validate-password`
 
 Tomei a liberdade de implementar uma segunda versão onde é retornada
 qual a regra não atendida pela senha:
 
`<domain:port>/rest/v2/validate-password`
 
Incluí neste projeto apenas os testes unitários. Os testes integrados
são feitos no Postman. Seu houver interesse, posso compartilhar a collection
com alguns testes realizados.
