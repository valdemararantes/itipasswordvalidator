package br.com.iti.api.rule

import br.com.iti.api.dto.ValidatePasswordInputDto
import br.com.iti.api.dto.ValidatePasswordOutputDto
import org.springframework.context.MessageSource
import java.util.*

abstract class ValidationRule(private val msgSrc: MessageSource, private val msgPropertyName: String) {
    abstract fun validate(inputDto: ValidatePasswordInputDto): ValidatePasswordOutputDto

    protected fun getBrokenRuleMessage() = msgSrc.getMessage(msgPropertyName, null, Locale.getDefault())
}