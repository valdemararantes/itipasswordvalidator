package br.com.iti.api.rule

import br.com.iti.api.dto.ValidatePasswordInputDto
import br.com.iti.api.dto.ValidatePasswordOutputDto
import org.slf4j.LoggerFactory
import org.springframework.context.MessageSource
import org.springframework.stereotype.Component
import java.util.regex.Pattern

@Component
class KindsOfCharsRule(messageSource: MessageSource) :
    ValidationRule(msgSrc = messageSource, msgPropertyName = "password_does_not_have_all_kind_of_chars") {

    companion object {
        private val log = LoggerFactory.getLogger(KindsOfCharsRule::class.java)

        val patterns: Array<Pattern> = arrayOf(
            Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE),
            Pattern.compile("[0-9]"),
            Pattern.compile("[a-z]"),
            Pattern.compile("[A-Z]")
        )
    }

    override fun validate(inputDto: ValidatePasswordInputDto): ValidatePasswordOutputDto {
        val password = inputDto.password.trim()

        val brokenPattern = patterns.firstOrNull { pattern ->
            !pattern.matcher(password).find()
        }

        return if (brokenPattern != null) {
            log.info("Pattern broken: $brokenPattern")
            ValidatePasswordOutputDto(valid = false, brokenRule = getBrokenRuleMessage())
        } else {
            ValidatePasswordOutputDto.OK
        }
    }
}