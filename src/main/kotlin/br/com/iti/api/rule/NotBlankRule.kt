package br.com.iti.api.rule

import br.com.iti.api.dto.ValidatePasswordInputDto
import br.com.iti.api.dto.ValidatePasswordOutputDto
import org.slf4j.LoggerFactory
import org.springframework.context.MessageSource
import org.springframework.stereotype.Component

@Component
class NotBlankRule(messageSource: MessageSource) :
    ValidationRule(msgSrc = messageSource, msgPropertyName = "password_cannot_be_blank") {

    companion object {
        private val log = LoggerFactory.getLogger(NotBlankRule::class.java)
    }

    override fun validate(inputDto: ValidatePasswordInputDto): ValidatePasswordOutputDto {
        return if (inputDto.password.isBlank()) {
            log.info("Rule broken: ${this.javaClass.name}")
            ValidatePasswordOutputDto(valid = false, brokenRule = getBrokenRuleMessage())
        } else {
            ValidatePasswordOutputDto.OK
        }
    }
}