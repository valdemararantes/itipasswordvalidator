package br.com.iti.api.service

import br.com.iti.api.config.MessageSourceConfig
import br.com.iti.api.dto.ValidatePasswordInputDto
import br.com.iti.api.rule.KindsOfCharsRule
import br.com.iti.api.rule.MinLengthRule
import br.com.iti.api.rule.NotBlankRule
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.ApplicationContext
import org.springframework.context.MessageSource

@SpringBootTest(
    classes = [ApplicationContext::class,
        PasswordValidatorService::class,
        MessageSourceConfig::class,
        MessageSource::class,
        NotBlankRule::class,
        MinLengthRule::class,
        KindsOfCharsRule::class]
)
internal class PasswordValidatorServiceTest {

    @Autowired
    private lateinit var pwdValidatorService: PasswordValidatorService

    @Test
    fun `validate WHEN password is perfect`() {
        val inputDto = ValidatePasswordInputDto(password = "Aa1#56789")
        val outputDto = pwdValidatorService.validate(inputDto)
        assertThat(outputDto.valid).isTrue()
    }

    @Test
    fun `validate WHEN password empty`() {
        val inputDto = ValidatePasswordInputDto(password = "")
        val outputDto = pwdValidatorService.validate(inputDto)
        assertThat(outputDto.valid).isFalse()
        assertThat(outputDto.brokenRule).isEqualTo("A senha não pode conter apenas brancos ou estar vazia")
    }

    @Test
    fun `validate WHEN password has blanks`() {
        val inputDto = ValidatePasswordInputDto(password = "   ")
        val outputDto = pwdValidatorService.validate(inputDto)
        assertThat(outputDto.valid).isFalse()
        assertThat(outputDto.brokenRule).isEqualTo("A senha não pode conter apenas brancos ou estar vazia")
    }

    @Test
    fun `validate WHEN password is short`() {
        val inputDto = ValidatePasswordInputDto(password = "aaa")
        val outputDto = pwdValidatorService.validate(inputDto)
        assertThat(outputDto.valid).isFalse()
        assertThat(outputDto.brokenRule).isEqualTo("A senha precisa ter no mínimo 9 caracteres")
    }

    @Test
    fun `validate WHEN password is short and padded with blanks`() {
        val inputDto = ValidatePasswordInputDto(password = "   aaa    ")
        val outputDto = pwdValidatorService.validate(inputDto)
        assertThat(outputDto.valid).isFalse()
        assertThat(outputDto.brokenRule).isEqualTo("A senha precisa ter no mínimo 9 caracteres")
    }

    @Test
    fun `validate WHEN lower case char is missing`() {
        val inputDto = ValidatePasswordInputDto(password = "AA1#56789")
        val outputDto = pwdValidatorService.validate(inputDto)
        assertThat(outputDto.valid).isFalse()
        assertThat(outputDto.brokenRule).isEqualTo("A senha precisa ter ao menos 1 dígito, 1 letra minúscula, 1 letra maiúscula e 1 caracter especial")
    }
}