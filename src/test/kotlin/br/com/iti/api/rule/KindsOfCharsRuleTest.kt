package br.com.iti.api.rule

import br.com.iti.api.config.MessageSourceConfig
import br.com.iti.api.dto.ValidatePasswordInputDto
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest(classes = [MessageSourceConfig::class, KindsOfCharsRule::class])
internal class KindsOfCharsRuleTest {

    @Autowired
    private lateinit var kindsOfCharsRule: KindsOfCharsRule

    @Test
    fun `validate WHEN has all kind of chars`() {
        val outputDto = kindsOfCharsRule.validate(ValidatePasswordInputDto("a1A!"))
        Assertions.assertThat(outputDto.valid).isTrue()
    }

    @Test
    fun `validate WHEN lower case char is missing`() {
        val outputDto = kindsOfCharsRule.validate(ValidatePasswordInputDto("1A!"))
        Assertions.assertThat(outputDto.valid).isFalse()
        Assertions.assertThat(outputDto.brokenRule).isEqualTo("A senha precisa ter ao menos 1 dígito, 1 letra minúscula, 1 letra maiúscula e 1 caracter especial")
    }

    @Test
    fun `validate WHEN upper case char is missing`() {
        val outputDto = kindsOfCharsRule.validate(ValidatePasswordInputDto("1a!"))
        Assertions.assertThat(outputDto.valid).isFalse()
        Assertions.assertThat(outputDto.brokenRule).isEqualTo("A senha precisa ter ao menos 1 dígito, 1 letra minúscula, 1 letra maiúscula e 1 caracter especial")
    }

    @Test
    fun `validate WHEN digit is missing`() {
        val outputDto = kindsOfCharsRule.validate(ValidatePasswordInputDto("Aa!"))
        Assertions.assertThat(outputDto.valid).isFalse()
        Assertions.assertThat(outputDto.brokenRule).isEqualTo("A senha precisa ter ao menos 1 dígito, 1 letra minúscula, 1 letra maiúscula e 1 caracter especial")
    }

    @Test
    fun `validate WHEN special char is missing`() {
        val outputDto = kindsOfCharsRule.validate(ValidatePasswordInputDto("Aa1"))
        Assertions.assertThat(outputDto.valid).isFalse()
        Assertions.assertThat(outputDto.brokenRule).isEqualTo("A senha precisa ter ao menos 1 dígito, 1 letra minúscula, 1 letra maiúscula e 1 caracter especial")
    }

}