package br.com.iti.api.controller

import br.com.iti.api.dto.ValidatePasswordInputDto
import br.com.iti.api.dto.ValidatePasswordOutputDto
import br.com.iti.api.service.PasswordValidatorService
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("rest")
class PasswordValidatorController(private val passwordValidatorService: PasswordValidatorService) {

    companion object {
        private val log = LoggerFactory.getLogger(PasswordValidatorController::class.java)
    }

    /**
     * Este é o método que foi pedido.
     */
    @PostMapping("v1/validate-password")
    fun validate(@RequestBody passwordInputDto: ValidatePasswordInputDto) =
        passwordValidatorService.validate(passwordInputDto).valid

    /**
     * Esta é uma segunda implementação, na qual um campo string, brokenRule, é retornado com a causa da
     * não validade da senha.
     */
    @PostMapping("v2/validate-password")
    fun validateWithDetail(@RequestBody passwordInputDto: ValidatePasswordInputDto): ValidatePasswordOutputDto {
        return passwordValidatorService.validate(passwordInputDto)
    }
}