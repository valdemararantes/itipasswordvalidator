package br.com.iti.api.controller

import br.com.iti.api.dto.ValidatePasswordInputDto
import br.com.iti.api.dto.ValidatePasswordOutputDto
import br.com.iti.api.service.PasswordValidatorService
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.nhaarman.mockitokotlin2.whenever
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status


@WebMvcTest
internal class PasswordValidatorTest() {

    private val validatePasswordUri = "/rest/v1/validate-password"
    private val mapper = ObjectMapper()

    @Autowired
    private lateinit var mvc: MockMvc

    @MockBean
    private lateinit var mockPwdValidatorService: PasswordValidatorService

    @Test
    fun test() {
        val pwd = "lalalala"
        val inputDto = ValidatePasswordInputDto(password = pwd)
        whenever(mockPwdValidatorService.validate(inputDto)).thenReturn(
            ValidatePasswordOutputDto(
                valid = false,
                brokenRule = "lalala não pode!"
            )
        )

        val inputJson: String = mapper.writeValueAsString(inputDto)
        val result = mvc.perform(
                post(validatePasswordUri)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(inputJson)
            )
            .andExpect(status().is2xxSuccessful)
            .andReturn()
        val outputDto = mapper.readValue<ValidatePasswordOutputDto>(result.response.contentAsString)
        assertThat(outputDto).isNotNull
        assertThat(outputDto.valid).isFalse()
        assertThat(outputDto.brokenRule).isNotEmpty()
    }
}
