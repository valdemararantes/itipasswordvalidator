package br.com.iti.api.rule

import br.com.iti.api.dto.ValidatePasswordInputDto
import br.com.iti.api.dto.ValidatePasswordOutputDto
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.MessageSource
import org.springframework.stereotype.Component

@Component
class MinLengthRule(messageSource: MessageSource) :
    ValidationRule(msgSrc = messageSource, msgPropertyName = "password_too_short") {

    companion object {
        private val log = LoggerFactory.getLogger(MinLengthRule::class.java)
    }

    @Value("\${app.min-length}")
    private var minLength = -1

    override fun validate(inputDto: ValidatePasswordInputDto): ValidatePasswordOutputDto {
        return if (inputDto.password.trim().length < minLength) {
            log.info("Rule broken: ${this.javaClass.name}")
            ValidatePasswordOutputDto(valid = false, brokenRule = getBrokenRuleMessage())
        } else {
            ValidatePasswordOutputDto.OK
        }
    }
}