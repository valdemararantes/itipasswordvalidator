package br.com.iti.api

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ITIApplication {
}

fun main(args: Array<String>) {
    runApplication<ITIApplication>(*args)
}
